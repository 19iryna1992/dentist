<?php if (have_rows('flexible_content')):

    while (have_rows('flexible_content')) : the_row();

        if (get_row_layout() == 'info_card'):

            get_template_part('template-parts/flexible-content/text-box');

        elseif (get_row_layout() == 'images_two_columns'):

            get_template_part('template-parts/flexible-content/images-two-columns');

        elseif (get_row_layout() == 'list_three_columns'):

            get_template_part('template-parts/flexible-content/list-three-columns');

        elseif (get_row_layout() == 'list_two_columns'):

            get_template_part('template-parts/flexible-content/text-list');

        elseif (get_row_layout() == 'wysiwig_content_with_image'):

            get_template_part('template-parts/flexible-content/text-image');

        endif;

    endwhile;

endif;