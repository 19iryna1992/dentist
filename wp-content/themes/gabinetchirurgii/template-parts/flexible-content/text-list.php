<?php
$title = get_sub_field('title');
$description = get_sub_field('description');

?>
<div class="c-info-card">
    <?php if ($title): ?>
        <div class="row p-0 m-0">
            <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                <div class="e-primary-title">
                    <h2 class="e-primary-title__item"><?= $title ?></h2>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row p-0 m-0">
        <?php if ($description): ?>
            <div class="col-12 col-lg-9 p-lg-0 m-lg-0">
                <div class="c-info-card__text">
                    <p><?= $description ?></p>
                </div>
            </div>
        <?php endif; ?>
        <?php if (have_rows('list')): ?>
            <div class="col-12">
                <div class="c-list c-list--two-column">
                    <?php while (have_rows('list')) : the_row(); ?>
                        <?php $item = get_sub_field('item'); ?>

                        <?php if ($item): ?>
                            <li class="c-list__item"> <?= $item ?></li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

