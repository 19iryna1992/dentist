<?php if (have_rows('images')): ?>
    <div class="c-info-card">
        <div class="row p-0 m-0">
            <?php while (have_rows('images')) : the_row(); ?>

                <?php $image = get_sub_field('img'); ?>

                <?php if ($image): ?>
                    <div class="col-12 col-md-6 mb-3 p-lg-0 m-lg-0">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>


