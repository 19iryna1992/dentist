<?php
$title = get_sub_field('title');
$intro = get_sub_field('intro');
?>
<div class="c-info-card">
    <?php if($intro): ?>
        <div class="col-12 p-lg-0 m-lg-0">
            <h2 class="c-info-card__intro"><?= $intro ?></h2>
        </div>
    <?php endif; ?>

    <?php if ($title): ?>
        <div class="row p-0 m-0">
            <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                <div class="e-primary-title">
                    <h2 class="e-primary-title__item"><?= $title ?></h2>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (have_rows('list')): ?>
        <div class="row  p-0 m-0">
            <div class="col-12">
                <ul class="c-list c-list--three-column">
                    <?php while (have_rows('list')) : the_row(); ?>
                        <?php $item = get_sub_field('item'); ?>

                        <?php if ($item): ?>
                            <li class="c-list__item"> <?= $item ?></li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>
</div>
