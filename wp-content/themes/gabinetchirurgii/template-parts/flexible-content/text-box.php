<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
?>

<div class="c-info-card">
    <?php if ($title): ?>
        <div class="row p-0 m-0">
            <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                <div class="e-primary-title">
                    <h2 class="e-primary-title__item"><?= $title ?></h2>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($text): ?>
        <div class="row p-0 m-0">
            <div class="col-12 col-lg-11  p-lg-0 m-lg-0">
                <div class="c-info-card-text">
                    <p><?= $text ?></p>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
