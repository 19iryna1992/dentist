<?php
$wysiwyg = get_sub_field('wysiwyg');
$image = get_sub_field('img');
$id_anchor = get_sub_field('id_section');
$title = get_sub_field('title');

?>

<?php if ($title): ?>
    <div class="row p-0 m-0">
        <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
            <div class="e-primary-title">
                <h2 class="e-primary-title__item"><?= $title ?></h2>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="c-info-card">
    <div class="scroll-section" id="<?= $id_anchor ? $id_anchor : '' ?>">
        <div class="row  justify-content-between p-0 m-0">
            <?php if ($wysiwyg): ?>
                <div class=" col-12 col-xl-7 p-lg-0 m-lg-0">
                    <?= $wysiwyg ?>
                </div>
            <?php endif; ?>
            <?php if ($image): ?>
                <div class=" d-none d-xl-block col-xl-4 p-0 m-0">
                    <div class="c-img--right">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
