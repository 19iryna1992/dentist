<div class="s-gallery">
    <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
            <div class="col-md-11 p-lg-0 m-lg-0">
                <div class="e-page-primary-title__box">
                    <h1 class="e-page-primary-title"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <div class="c-page-content">
            <div class="row p-0 m-0">
                <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                    <div class="e-primary-title">
                        <h2 class="e-primary-title__item"></h2>
                    </div>
                </div>
            </div>
            <?php get_template_part('template-parts/components/gallery-thumbnail'); ?>
        </div>
    </div>
</div>
