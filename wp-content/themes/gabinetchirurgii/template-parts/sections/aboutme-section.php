<?php get_template_part('template-parts/components/modal-window'); ?>
<div class="s-aboutme">
    <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
            <div class="col-md-11 p-lg-0 m-lg-0">
                <div class="e-page-primary-title__box">
                    <h1 class="e-page-primary-title"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <div class="c-page-content">
            <?php get_template_part('template-parts/flexible-content/flexible-content'); ?>
        </div>
    </div>
</div>