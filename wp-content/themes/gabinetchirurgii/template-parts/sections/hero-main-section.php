<?php
$banner_title = get_field('banner_title');
$banner_description = get_field('banner_description');

?>

<div class="c-hero-main">
    <div class="container-fluid p-0 m-0">
        <div class="c-hero-main-position">
            <divclass="row p-0 m-0 justify-content-end">
                <?php if ($banner_title): ?>
                    <div class="col-12 offset-md-1 col-md-11">
                        <h1 class="e-hero-main__title e-hero-main__title--right"><?= $banner_title ?></h1>
                    </div>
                <?php endif; ?>

                <?php if ($banner_description): ?>
                    <div class="col-12 offset-md-1 col-md-11">
                        <?= $banner_description ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

    

