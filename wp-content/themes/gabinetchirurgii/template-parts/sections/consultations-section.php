<?php
$intro_text = get_field('intro_text');
?>
<div class="s-consultation">
    <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
            <div class="col-md-11 p-lg-0 m-lg-0">
                <div class="e-page-primary-title__box">
                    <h1 class="e-page-primary-title"><?php the_title(); ?></h1>

                    <?php if($intro_text): ?>
                        <p class="c-consultations-main-description">
                            <?= $intro_text?>
                        </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="c-page-content">
            <?php get_template_part('template-parts/flexible-content/flexible-content'); ?>
        </div>

    </div>
</div>
