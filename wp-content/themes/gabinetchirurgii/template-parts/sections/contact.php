<div class="s-contact">
    <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
            <div class="col-md-11 p-lg-0 m-lg-0">
                <div class="e-page-primary-title__box">
                    <h1 class="e-page-primary-title"><?php the_title(); ?></h1>
                </div>
            </div>
            <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                <div class="e-component-title-wrap" style="padding-left: 0;">
                    <h2 class="e-primary-title"></h2>
                </div>
            </div>
        </div>
        <div class="c-page-content">
            <?php get_template_part('template-parts/components/wysiwyg-with-img');  ?>
            <?php get_template_part('template-parts/components/map');  ?>

        </div>
    </div>
</div>
