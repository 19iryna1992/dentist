<?php
$title = get_field('title_partners');
?>
<div class="s-partners">
    <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
            <div class="col-md-11 p-lg-0 m-lg-0">
                <div class="e-page-primary-title__box">
                    <h1 class="e-page-primary-title"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <div class="c-page-content">
            <?php if ($title): ?>
                <div class="row p-0 m-0">
                    <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                        <div class="e-primary-title">
                            <h2  class="e-primary-title__item"><?= $title ?></h2>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (have_rows('partners')): ?>
                <div class="c-images-two-column">
                    <div class="row p-0 m-0">
                        <?php while (have_rows('partners')) : the_row(); ?>
                            <?php get_template_part('template-parts/components/partner'); ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
