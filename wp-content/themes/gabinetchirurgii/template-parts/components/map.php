<?php
$map = get_field('map')
?>

<?php if($map ): ?>
    <div class="row p-0 m-0">
        <div class="col-12 col-lg-11">
            <div class="c-map ">
                <?= $map?>
            </div>
        </div>
    </div>

<?php endif; ?>
