<?php
$icon = get_field('call_icon', 'option');
$text = get_field('call_text', 'option');
$phone = get_field('phone', 'option');

$position = '';
?>

<?php if (is_page_template('template-page/home-template.php')): ?>
    <?php $position = 'style="right: 0"'; ?>
<?php endif; ?>

<div id="actions_phone" class="c-actions-call" <?= $position ?> >
    <?php if ($icon): ?>
        <div id="actions_icon" class="c-actions-call__icon">
            <img src="<?= $icon['url'] ?>" alt="<?= $icon['alt'] ?>">
        </div>
    <?php endif; ?>

    <div class="c-actions-phone-wrap">
        <?php if ($text): ?>
            <p class="c-actions-phone__text"><?= $text ?></p>
        <?php endif; ?>

        <?php if ($phone): ?>
            <div class="c-actions-phone-wrap">
                <a class="c-actions-phone__link" href="tel:<?= $phone ?>"><?= $phone ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>
