<?php
$wysiwyg= get_field('contact_wysiwyg');
$image = get_field('contact_image');

?>

<div class="c-wysiwyg-img">
    <div class="row p-0 m-0">
        <?php if ($wysiwyg): ?>
            <div class="col-6 col-lg-5">
                <div class="">
                    <?= $wysiwyg ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($image): ?>
            <div class="col-6">
                <div class="" style="float:right">
                    <img src="<?= $image['url']?>" alt="<?= $image['alt']?>">
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
