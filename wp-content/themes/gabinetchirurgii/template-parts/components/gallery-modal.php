<?php if (have_rows('gallery_modal')): ?>

    <?php while (have_rows('gallery_modal')) :
        the_row(); ?>

        <?php
        $id_anchor = get_sub_field('id_gallery');
        $title = get_sub_field('gallery_name');
        $images = get_sub_field('gallery');
        ?>
        <div class="c-info-card">
            <div class="scroll-section" id="<?= $id_anchor ? $id_anchor : '' ?>">
                <?php if ($title): ?>
                    <div class="row p-0 m-0">
                        <div class="col-12 pr-0 mr-0 p-lg-0 m-lg-0">
                            <div class="e-primary-title">
                                <h2 id="<?= $id_anchor ? $id_anchor : '' ?>"
                                    class="e-primary-title__item"><?= $title ?></h2>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="c-gallery-modal JS--gallery">
                    <?php if ($images): ?>
                        <?php foreach ($images as $image): ?>
                            <img class="c-gallery-modal__img JS--gallery-modal_img" src="<?= $image['url']; ?>"
                                 alt="<?= $image['alt']; ?>"/>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>



