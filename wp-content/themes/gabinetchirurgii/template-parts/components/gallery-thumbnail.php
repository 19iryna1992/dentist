<?php $images = get_field('galeria'); ?>
<div class="l-gallery-thumbnail">
    <div class="row p-0 m-0">
        <div class="col-12 col-lg-6 p-lg-0 m-lg-0 mb-3 pr-lg-3 ">
            <div class=" l-gallery-thumbnai-img-box">
                <img id="active-gallery-thumbnail-img" src="" alt="">
            </div>
        </div>
        <div class="col-12 col-lg-6 p-lg-0 m-lg-0">
            <div class="row p-0 m-0">
                <?php if ($images): ?>
                    <?php foreach ($images as $image): ?>
                        <div class="col-4 p-2 m-0 mb-2">
                            <img class="c-gallery-thumbnail JS--gallery-thumbnail" src="<?= $image['url']; ?>"
                                 alt="<?= $image['alt']; ?>"/>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
