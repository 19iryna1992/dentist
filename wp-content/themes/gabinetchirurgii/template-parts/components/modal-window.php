<?php ?>

<div class="c-modal-overlay JS--portfolio-overlay"></div>
<div class="c-portfolio-modal JS--gallery-modal">
    <img id="active-image" src="" alt="">
    <div id="left">
        <i  class="fas fa-chevron-circle-left"></i>
    </div>
    <div id="btn-close">
        <i class="fas fa-times-circle"></i>
    </div>
    <div id="right">
        <i  class="fas fa-chevron-circle-right"></i>
    </div>
</div>

