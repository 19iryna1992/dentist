<?php
$partner = get_sub_field('partner_name');
$logo = get_sub_field('partner_logo');

?>
<div class="col-12 col-md-6">
    <?php if ($partner): ?>
        <p><?= $partner ?></p>
    <?php endif; ?>
    <?php if ($logo): ?>
        <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
    <?php endif; ?>

</div>