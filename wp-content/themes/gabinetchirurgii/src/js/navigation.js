import anime from 'animejs';

(function ($) {

    $(document).ready(function () {
        menuItemActive();

        function menuItemActive() {
            const links = document.querySelectorAll('.menu-item a');
            links.forEach(e => {
                var targetlink = e.getAttribute('href');
                if (targetlink === window.location.href) {
                    e.parentElement.classList.add('active-item');

                } else {
                    e.parentElement.classList.remove('active-item');
                }
            });
        }

        //Toggle menu

        $('#menu_toggle').click(function () {

            var toggleIcon = $('#menu_toggle').children('svg');

            if (toggleIcon.hasClass('fa-times')) {
                toggleIcon.removeClass('fa-times');
                toggleIcon.addClass('fa-bars');

            } else if (toggleIcon.hasClass('fa-bars')) {
                toggleIcon.removeClass('fa-bars');
                toggleIcon.addClass('fa-times');
            }

            $('#show_hide_sidebar').toggleClass('show-sidebar');
        });

        //Action call

        $('#actions_icon').click(function () {

            $('#actions_phone').toggleClass('c-actions-phone--open');
        });


        //dropdown
        var menuItemWithChildren = $('.menu-item-has-children a ');

        if (menuItemWithChildren.attr("href") == $(location).attr('href')) {
            $('.sub-menu').toggleClass('sub-menu--open');
            $('.sub-menu').parent().addClass('height-auto');
            $('.sub-menu').prepend("<span class='active-anchor'></span>");
            $('.sub-menu li').first().addClass('is-active');


            //smoothscroll

            var $page = $('html, body');

            $('.sub-menu a[href*="#"]').click(function (e) {

                e.preventDefault();

                $(".is-active").toggleClass("is-active");
                $(this).parent().toggleClass("is-active");

                var newY = $(this).parent().position().top;

                anime({
                    targets: ".active-anchor",
                    translateY: newY,
                    duration: 900,
                    easing: 'easeInOutExpo',
                });

                $page.animate({
                    scrollTop: $($.attr(this, 'href')).offset().top
                }, 600);
                return false;
            });

        }

    });


    $(window).scroll(function () {
        var scrollDistance = $(window).scrollTop();
        $('.scroll-section').each(function (i) {
            let id = $(this).attr('id');
            if ($(this).position().top <= scrollDistance) {
                $(".is-active").removeClass("is-active");
                $('.sub-menu a[href="#' + id + '"]').parent().addClass('is-active');
            }
        });
        var newY = $('.is-active').position().top;
        anime({
            targets: ".active-anchor",
            translateY: newY,
            duration: 900,
            easing: 'easeInOutExpo',
        });

    }).scroll();

})(jQuery);

