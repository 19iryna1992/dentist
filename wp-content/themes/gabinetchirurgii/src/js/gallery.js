document.addEventListener('DOMContentLoaded', (event) => {

    const galleryThumbnail = document.querySelectorAll('.JS--gallery-thumbnail');
    if (galleryThumbnail != null) {
        const galleryThumbnailActive = document.getElementById('active-gallery-thumbnail-img');

        galleryThumbnailActive.src = galleryThumbnail[0].src;


        galleryThumbnail.forEach(img => {
            img.addEventListener('click', showGalleryImage)
        });

        function showGalleryImage(e) {
            galleryThumbnailActive.src = e.target.src;

        }
    }
});