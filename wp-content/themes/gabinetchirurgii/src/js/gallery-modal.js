document.addEventListener('DOMContentLoaded', (event) => {

    const galleries = document.querySelectorAll('.JS--gallery');

    const modalBox = document.querySelector('.JS--gallery-modal');
    const activeImage = document.getElementById('active-image');
    const overlayBox = document.querySelector('.JS--portfolio-overlay');
    const btnClose = document.getElementById('btn-close');
    const btnSlideLeft = document.getElementById('left');
    const btnSlideRight = document.getElementById('right');

    galleries.forEach(gallery => {
        var galleryImgs = gallery.querySelectorAll('.JS--gallery-modal_img');

        if (galleryImgs != null) {
            galleryImgs.forEach(img => {
                img.addEventListener('click', showGalleryImage)
            });
        }

        function showGalleryImage(e) {
            activeImage.src = e.target.src;
            overlayBox.style.display = 'block';
            modalBox.style.display = 'block';

        }

        btnClose.addEventListener('click', function () {
            overlayBox.style.display = 'none';
            modalBox.style.display = 'none';
        });

        btnSlideRight.addEventListener('click', function () {
            for (var i = 0; i < galleryImgs.length; i++) {
                if (activeImage.src === galleryImgs[i].src) {
                    var nextImg = galleryImgs[i].nextElementSibling;
                    console.log(nextImg);
                }
            }
            if (nextImg === null) {
                activeImage.src = galleryImgs[0].src
            } else {
                activeImage.src = nextImg.src;
            }
        });

        btnSlideLeft.addEventListener('click', function () {
            for (var i = 0; i < galleryImgs.length; i++) {
                if (activeImage.src === galleryImgs[i].src) {
                    var nextImg = galleryImgs[i].previousElementSibling;
                    console.log(nextImg);
                }
            }
            if (nextImg === null) {
                activeImage.src = galleryImgs[galleryImgs.length - 1].src
            } else {
                activeImage.src = nextImg.src;
            }
        })

    });

});