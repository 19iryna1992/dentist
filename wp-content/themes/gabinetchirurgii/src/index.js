import Swiper from 'swiper';
import 'bootstrap';
import './js/gallery';
import 'animejs/lib/anime'
import './js/navigation.js';
import './js/gallery-modal.js';
import './sass/style.scss';
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';

