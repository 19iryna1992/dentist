<?php /* Template Name: Start */

get_header(); ?>
<?php
$background_img = get_field('background_img');
$background_img_mobile = get_field('background_img_mobile');
?>

<style>
    .b-site-content{
        background-image: url(<?php echo $background_img_mobile['url']?>);
        background-position-y: 70px;
        background-position-x: 50%;
    }
    @media screen and (min-width: 992px) {
        .b-site-content{
            background-position-y: 0;
        }
    }
    @media screen and (min-width: 1200px) {
        .b-site-content{
            background-image: url(<?php echo $background_img['url']?>);
            background-position-x: 0;
        }
    }

</style>

<main id="main" role="main" class="b-main" tabindex="-1" >
    <div class="b-site-content">
        <?php get_template_part('template-parts/sections/hero-main-section'); ?>
    </div>

</main>
<!-- close tags opening in header.php-->

</div>
</body>
</html>