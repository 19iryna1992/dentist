<?php /* Template Name: Galeria */

get_header(); ?>
<main id="main" role="main" class="b-main" tabindex="-1">
    <div id="content" class="b-site-content">
        <?php get_template_part('template-parts/sections/gallary-section'); ?>
    </div>
</main>
<!-- close tags openingin header.php-->
</div>
</body>
</html>