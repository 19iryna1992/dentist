<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GabinetChirurgii
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="b-site">
    <?php $logo = get_field('logo', 'option'); ?>
    <header id="masthead" class="b-header">

        <div class="d-lg-none w-100  position-relative">
            <div class="c-mobile-branding ">
                <button id="menu_toggle" class="menu-toggle"><i class="fas fa-bars"></i></button>
                <?php if ($logo): ?>
                    <a href="<?= get_site_url() ?>">
                        <div class="c-mobil__logo">
                            <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>" class="src">
                        </div>
                    </a>
                <?php endif; ?>
            </div>
        </div>

        <div class="c-header-sidebar">
            <div id="show_hide_sidebar">
                <?php if ($logo): ?>
                    <a href="<?= get_site_url() ?>">
                        <div class="c-header__branding">
                            <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>" class="src">
                        </div>
                    </a>
                <?php endif; ?>

                <nav class="c-header-navigation">
                    <div class="c-primary-navigation">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'menu-1',
                            'menu_class' => 'c-header-navigation__items',
                        ));
                        ?>
                    </div>
                </nav>
                <footer class="c-header-footer">
                    <?php the_field('footer_text', 'option'); ?>
                </footer>
            </div>
        </div>
    </header>


    <?php get_template_part('template-parts/components/actions-call'); ?>
	
